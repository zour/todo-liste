import { Injectable } from '@angular/core';
import { ITodo } from '../pages/ajouter/ajouter.component';

@Injectable({
  providedIn: 'root'
})
export class TodoServiceService {

  
  // Placeholder for todo's
  todos: ITodo[] = [];
  /** Used to generate unique ID's */
  nextId = 0;
  constructor() { }
  // Simulate POST /todos
  addTodo(todo: ITodo): ITodo {
    todo.id = ++this.nextId;
    this.todos.unshift(todo);
    return todo;
  }

  // Simulate DELETE /todos/:id
  deleteTodoById(id: number): TodoServiceService {
    this.todos = this.todos.filter(todo => todo.id !== id);
    return this;
  }

  // Simulate POST /todos/delete
  deleteAllTodo(): TodoServiceService {
    this.todos = this.todos
      .filter(todo => !todo.doneDate);
    return this;
  }

  // Simulate PUT /todos/:id
  updateTodoById(id: number, values: Object = {}): ITodo {
    const todo = this.getTodoById(id);
    if (!todo) {
      return null;
    }
    Object.assign(todo, values);
    return todo;
  }

  // Simulate GET /todos
  getAllTodos(): ITodo[] {
    return this.todos;
  }

  // Simulate GET /todos/:id
  getTodoById(id: number): ITodo {
    return this.todos.filter(todo => todo.id === id).pop();
  }

}
