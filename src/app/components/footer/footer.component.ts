import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {

  constructor(private router: Router,
) {}
   url!:string;

  ngOnInit() {
     this.getUrl();
      this.url = location.pathname;
    } 

  getUrl(){
    console.log(this.url)
      if( this.router.url.includes(''))
    {
      this.url='';
      
    }
    else if( this.router.url.includes('/ajouter'))
    {

      this.url='/ajouter';
      console.log(this.url);
    }
    else if( this.router.url.includes('historique'))
    {

      this.url='historique';
    }
  }
}
