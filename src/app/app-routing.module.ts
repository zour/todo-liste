import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AjouterComponent } from './pages/ajouter/ajouter.component';
import { HistoriqueComponent } from './pages/historique/historique.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
 
  {
    path:"home",
    component: HomeComponent
    },
  {
  path:"ajouter",
  component: AjouterComponent
  },
  {
    path:"historique",
    component: HistoriqueComponent
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
