import { Component, OnInit } from '@angular/core';
import { FormGroup,} from '@angular/forms';
import { Router } from '@angular/router';
import { TodoServiceService } from 'src/app/service/todo-service.service';
interface Categorie {
  name: string;
  icon:string;
  checked: boolean;
}
type CategoryType = "shopping" | "health" | "work" | "bills" | "cleaning" | "other";

export interface ITodo {
    id: number;
    content: string;
    category: CategoryType;
    isUrgent: boolean;
    doneDate: Date | null;
}
const LIST_FROM_DATABASE = [
{ name:"Shopping",icon: '🛍️',  checked: false },
{ name:"Health",icon: '💊️', checked: false },
{ name:"Work",icon:'💼', checked: false },
{ name:"Bills",icon:'💸', checked: false },
{ name:"cleaning",icon: '🧼', checked: false },
{ name:"Other",icon: '🤷‍♀️', checked: false },
];
@Component({
  selector: 'app-ajouter',
  templateUrl: './ajouter.component.html',
  styleUrls: ['./ajouter.component.css']
})
export class AjouterComponent implements OnInit {
  name!: string ;
  categories: Categorie[]=LIST_FROM_DATABASE;
  form!: FormGroup;
  mytextarea!: string;
  
  tache:ITodo={ 
    id: 0,
    content: "",
    category: "cleaning",
    isUrgent: false,
    doneDate: null,
  }
  constructor(private router: Router , private todoService: TodoServiceService ) { }
  
 
    ngOnInit() {
      this.loadAllTodoList();
    }
    getContent(description: string) {
      this.tache.content = description;
     } 
     categorieChecked(name:string,checked:boolean){
       this.categories.update(name,{done:checked});
   
     } 
    loadAllTodoList() {
       this.todoService.getAllTodos();
    }
  
    EditTodoDetail() {
      this.router.navigate(['/home'], {queryParams:{id:id}});
    }
  
    AddTodo() {
      this.router.navigate(['/ajouter']);
    }
    TodoDelete() {
      this.todoService.deleteAllTodo();
      this.loadAllTodoList();
    }
  }

