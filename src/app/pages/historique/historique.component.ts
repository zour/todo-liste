import { Component } from '@angular/core';
import { HistoriqueToDoService } from 'src/app/service/historique-to-do.service';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.component.html',
  styleUrls: ['./historique.component.css']
})
export class HistoriqueComponent {
  historique: HistoriqueToDoService[] = [];
}
