import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { AjouterComponent } from './pages/ajouter/ajouter.component';
import { HistoriqueComponent } from './pages/historique/historique.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { TodoServiceComponent } from './service/todo-service/todo-service.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AjouterComponent,
    HistoriqueComponent,
    FooterComponent,
    HomeComponent,
    TodoServiceComponent,
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
